﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;

namespace Ex4Start
{
    public class Person
    {
        public string Name { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        public double BMI
        {
            get { return Weight / Height / Height; }
        }

        public override string ToString()
        {
            string s = $"{Name} højde={Height}m. vægt={Weight}kg. BMI: {BMI:##.#}";
            return s;
        }

    }


}
